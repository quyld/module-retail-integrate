<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 20/03/2017
 * Time: 18:51
 */

namespace SM\Integrate\Helper;


/**
 * Class Data
 *
 * @package SM\Integrate\Helper
 */
class Data {

    /**
     * @return bool
     */
    public function isIntegrateRP() {
        return true;
    }

    /**
     * @return bool
     */
    public function isIntegrateWH() {
        return false;
    }
}